<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>test suite api</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>98b8e6a3-426a-4ad7-8bda-69f40d731fd6</testSuiteGuid>
   <testCaseLink>
      <guid>41f9a4de-e571-4b0a-866a-6a883dda0a45</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CCFN/API testing/CCFN - Parametre ID-CONT des fonctions Controler et Detruire</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4d8cfcf8-1c03-460e-af8d-20e0ccea546a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CCFN/API testing/CCFN Deposer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6092c1dd-c535-4aa0-be5f-aff313191555</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CCFN/API testing/CCFN WS Compter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>de85eab7-e3e7-4584-9ad9-29f4328302b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CCFN/API testing/CCFN WS Controler</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5b831d8-4f51-4b88-bb17-5f60ad81444c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CCFN/API testing/CCFN WS Deposer un document existant</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c630825-da95-4ec0-a74a-b39b6a1c37ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CCFN/API testing/CCFN WS Detruire</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42b3ece2-3eef-4301-a67a-e349a9319f2b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CCFN/API testing/CCFN WS Lire document</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b570b48f-1252-4e7e-83b3-810f81f0259f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CCFN/API testing/CCFN WS Lire journal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>193fe59d-306a-4c51-ab49-889e29451c6a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CCFN/API testing/CCFN WS Lire metadonnees techniques</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6748821c-e331-4a9a-8421-bcf54169c129</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CCFN/API testing/CCFN WS Lister</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
