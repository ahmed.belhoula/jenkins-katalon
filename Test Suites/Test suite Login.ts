<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test suite Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>15</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b5100bfe-b080-41d9-af83-f1096eaa80d3</testSuiteGuid>
   <testCaseLink>
      <guid>892723ae-1f18-4564-a94c-fc0775c01da2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Page/Login successfully with username and password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12724047-35d9-4cd9-9252-9bdd9ea95a4c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Page/Login successfully with username and password specified in GlobalVariable</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a33bfe57-ab6b-4ccc-87e1-d38f81b794e3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fb00b5b9-1a2b-4858-b3b6-6a40b5681779</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1f3fa781-fb02-4be2-b998-7b8ded291be3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Page/login with empty username and empty password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>47b01a02-b93a-4ff2-8cf7-da8e750e96b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Page/login with invalid username and invalid password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>231721a3-f7eb-4109-8adb-cddc257541b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Page/Login with invalid username and valid password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df68b76d-34a3-48e9-86f5-27bbe54d7942</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Page/Login with valid username and invalid password</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
