<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Detruire sans id-cont</name>
   <tag></tag>
   <elementGuidId>2f0071f8-b5f0-4839-b673-b82875b75a57</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;id-ccfn&quot;,
      &quot;value&quot;: &quot;${id-ccfn}&quot;,
      &quot;type&quot;: &quot;Text&quot;
    },
    {
      &quot;name&quot;: &quot;id-uti&quot;,
      &quot;value&quot;: &quot;${id-uti}&quot;,
      &quot;type&quot;: &quot;Text&quot;
    },
    {
      &quot;name&quot;: &quot;idu&quot;,
      &quot;value&quot;: &quot;${idu}&quot;,
      &quot;type&quot;: &quot;Text&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Basic bmF0YXNoYUBnbWFpbC5jb206QE9wZW5iZWU3NA==</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>DELETE</restRequestMethod>
   <restUrl>${url}/ws/ccfn/document?id-ccfn=${id-ccfn}&amp;id-uti=${id-uti}&amp;idu=${idu}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.URL</defaultValue>
      <description></description>
      <id>8d67376d-aa06-4290-8781-3524c8ef6af6</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.id_ccfn</defaultValue>
      <description></description>
      <id>a64c2705-dd6a-4797-a83e-f471af457396</id>
      <masked>false</masked>
      <name>id-ccfn</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.id_uti</defaultValue>
      <description></description>
      <id>e15e0a1d-6acc-491e-85b1-f0e90942c71f</id>
      <masked>false</masked>
      <name>id-uti</name>
   </variables>
   <variables>
      <defaultValue>219</defaultValue>
      <description></description>
      <id>f5f4e0e0-161d-42d4-ae86-77f06e2f6aef</id>
      <masked>false</masked>
      <name>idu</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)

idCont = response.getHeaderFields()['Id-Cont'][0]

assertThat(response.getHeaderFields()['Id-Cont'][0]).isEqualTo(idCont)

idU = response.getHeaderFields()['Idu'][0]

assertThat(response.getHeaderFields()['Idu'][0]).isEqualTo(idU)

idCcfn = response.getHeaderFields()['Id-Ccfn'][0]

assertThat(response.getHeaderFields()['Id-Ccfn'][0]).isEqualTo(idCcfn)

idOnUti = response.getHeaderFields()['Id-On-Uti'][0]

assertThat(response.getHeaderFields()['Id-On-Uti'][0]).isEqualTo(idOnUti)

idUti = response.getHeaderFields()['Id-Uti'][0]

assertThat(response.getHeaderFields()['Id-Uti'][0]).isEqualTo(idUti)

size = response.getHeaderFields()['Size'][0]

assertThat(response.getHeaderFields()['Size'][0]).isEqualTo(size)

hashLog = response.getHeaderFields()['Hash-Algo'][0]

assertThat(response.getHeaderFields()['Hash-Algo'][0]).isEqualTo(hashLog)

hash = response.getHeaderFields()['Hash'][0]

assertThat(response.getHeaderFields()['Hash'][0]).isEqualTo(hash)
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
