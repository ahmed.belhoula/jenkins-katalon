<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Lire journal from start to end date</name>
   <tag></tag>
   <elementGuidId>6bd6592f-6dc5-48aa-af83-ae71f2b222d9</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Basic bmF0YXNoYUBnbWFpbC5jb206QE9wZW5iZWU3NA==</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>${url}/ws/ccfn/ledger?id-ccfn=${id-ccfn}&amp;id-cont=${id-cont}&amp;start-date=${start-date}&amp;end-date=${end-date}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.URL</defaultValue>
      <description></description>
      <id>b1d2e5ee-0260-4ef9-844a-bdd240190d3c</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.id_ccfn</defaultValue>
      <description></description>
      <id>491ee926-f110-4dd1-ac44-803a0f30b2df</id>
      <masked>false</masked>
      <name>id-ccfn</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.id_cont</defaultValue>
      <description></description>
      <id>33fa45ec-2bed-473e-b235-2beaa5e7ce4f</id>
      <masked>false</masked>
      <name>id-cont</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>17577516-6b9a-4dcd-9e15-6a7dd1ee28e4</id>
      <masked>false</masked>
      <name>start-date</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>608f2572-2a0b-4010-8ea5-b77114b358b8</id>
      <masked>false</masked>
      <name>end-date</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
