<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Check document</name>
   <tag></tag>
   <elementGuidId>79cb9548-50b3-4af0-abad-4921b805d384</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Basic bmF0YXNoYUBnbWFpbC5jb206QE9wZW5iZWU3NA==</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>${url}/ws/ccfn/document/check?id-uti=${id-uti}&amp;id-ccfn=${id-ccfn}&amp;idu=${idu}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.URL</defaultValue>
      <description></description>
      <id>34bdc7e9-667d-4259-848c-e60fb179ab62</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.id_uti</defaultValue>
      <description></description>
      <id>a41ba6e0-2cad-49b9-8c33-aeb7655f2b1f</id>
      <masked>false</masked>
      <name>id-uti</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.id_ccfn</defaultValue>
      <description></description>
      <id>06228d5a-0943-4e4d-897c-72f3bff77bee</id>
      <masked>false</masked>
      <name>id-ccfn</name>
   </variables>
   <variables>
      <defaultValue>0</defaultValue>
      <description></description>
      <id>f2744cdc-56c3-488f-ae08-d5c8666d5438</id>
      <masked>false</masked>
      <name>idu</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

idU = response.getHeaderFields()['Idu'][0]

assertThat(response.getHeaderFields()['Idu'][0]).isEqualTo(idU)

idCcfn = response.getHeaderFields()['Id-Ccfn'][0]

assertThat(response.getHeaderFields()['Id-Ccfn'][0]).isEqualTo(idCcfn)

idOnUti = response.getHeaderFields()['Id-On-Uti'][0]

assertThat(response.getHeaderFields()['Id-On-Uti'][0]).isEqualTo(idOnUti)

idUti = response.getHeaderFields()['Id-Uti'][0]

assertThat(response.getHeaderFields()['Id-Uti'][0]).isEqualTo(idUti)
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
