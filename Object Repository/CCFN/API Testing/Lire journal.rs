<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Lire journal</name>
   <tag></tag>
   <elementGuidId>05697a3f-e532-44ae-9e2e-cb576c1bd4eb</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Basic bmF0YXNoYUBnbWFpbC5jb206QE9wZW5iZWU3NA==</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>${url}/ws/ccfn/ledger?id-ccfn=${id-ccfn}&amp;id-cont=${id-cont}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.id_ccfn</defaultValue>
      <description></description>
      <id>a5190bc4-46ba-4cd0-a104-74b82b63ef61</id>
      <masked>false</masked>
      <name>id-ccfn</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.id_cont</defaultValue>
      <description></description>
      <id>cb4b7aec-3ff7-478f-b741-64970e4834c1</id>
      <masked>false</masked>
      <name>id-cont</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.URL</defaultValue>
      <description></description>
      <id>6ee7087f-904e-4ad3-b89f-db317129300e</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

assertThat(response.getStatusCode()).isIn(Arrays.asList(200, 201, 202))

WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)

idCont = response.getHeaderFields()['Id-Cont'][0]

assertThat(response.getHeaderFields()['Id-Cont'][0]).isEqualTo(idCont)

idCcfn = response.getHeaderFields()['Id-Ccfn'][0]

assertThat(response.getHeaderFields()['Id-Ccfn'][0]).isEqualTo(idCcfn)

idUti = response.getHeaderFields()['Id-Uti'][0]

assertThat(response.getHeaderFields()['Id-Uti'][0]).isEqualTo(idUti)

date = response.getHeaderFields()['Date'][0]

assertThat(response.getHeaderFields()['Date'][0]).isEqualTo(date)



</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
