<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Lire journal avec id-u</name>
   <tag></tag>
   <elementGuidId>58d688ce-585e-4711-a349-ab7e66634da4</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Basic bmF0YXNoYUBnbWFpbC5jb206QE9wZW5iZWU3NA==</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>${url}/ws/ccfn/ledger?id-ccfn=${id-ccfn}&amp;id-cont=${id-cont}&amp;idu=${idu}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.id_ccfn</defaultValue>
      <description></description>
      <id>c61e7ddb-1d53-4ea7-81ab-8b975caa7cce</id>
      <masked>false</masked>
      <name>id-ccfn</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.id_cont</defaultValue>
      <description></description>
      <id>24bffebf-6db4-47d6-ae99-03b353b45fbb</id>
      <masked>false</masked>
      <name>id-cont</name>
   </variables>
   <variables>
      <defaultValue>200</defaultValue>
      <description></description>
      <id>b95d0195-9695-4a1c-bbc8-7e4b6d2fae9c</id>
      <masked>false</masked>
      <name>idu</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.URL</defaultValue>
      <description></description>
      <id>8a2ead71-6162-45d4-905d-c1114f25b04e</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

assertThat(response.getStatusCode()).isIn(Arrays.asList(200, 201, 202))

WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
