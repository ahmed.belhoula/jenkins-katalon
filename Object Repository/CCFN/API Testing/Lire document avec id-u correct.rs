<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Lire document avec id-u correct</name>
   <tag></tag>
   <elementGuidId>5e27067f-9921-4bae-817b-3c04a35ff00f</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Basic bmF0YXNoYUBnbWFpbC5jb206QE9wZW5iZWU3NA==</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>${url}/ws/ccfn/file?idu=${idu}&amp;id-ccfn=${id-ccfn}&amp;id-cont=${id-cont}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.URL</defaultValue>
      <description></description>
      <id>a8b37d56-34bf-4bbf-ac72-6b0133332a52</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.id_ccfn</defaultValue>
      <description></description>
      <id>2aee9720-dc9e-4dfd-b310-96a4eccdd96f</id>
      <masked>false</masked>
      <name>id-ccfn</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.id_cont</defaultValue>
      <description></description>
      <id>21dedf7e-a120-468f-9160-919b2ffd2d2b</id>
      <masked>false</masked>
      <name>id-cont</name>
   </variables>
   <variables>
      <defaultValue>0</defaultValue>
      <description></description>
      <id>c4b0ab3a-5bf9-493c-9342-5f12f09c24f3</id>
      <masked>false</masked>
      <name>idu</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

idCont = response.getHeaderFields()['Id-Cont'][0]

assertThat(response.getHeaderFields()['Id-Cont'][0]).isEqualTo(idCont)

idU = response.getHeaderFields()['Idu'][0]

assertThat(response.getHeaderFields()['Idu'][0]).isEqualTo(idU)

idCcfn = response.getHeaderFields()['Id-Ccfn'][0]

assertThat(response.getHeaderFields()['Id-Ccfn'][0]).isEqualTo(idCcfn)

idOnUti = response.getHeaderFields()['Id-On-Uti'][0]

assertThat(response.getHeaderFields()['Id-On-Uti'][0]).isEqualTo(idOnUti)

idUti = response.getHeaderFields()['Id-Uti'][0]

assertThat(response.getHeaderFields()['Id-Uti'][0]).isEqualTo(idUti)

date = response.getHeaderFields()['Date'][0]

assertThat(response.getHeaderFields()['Date'][0]).isEqualTo(date)

size = response.getHeaderFields()['Size'][0]

assertThat(response.getHeaderFields()['Size'][0]).isEqualTo(size)

hashLog = response.getHeaderFields()['Hash-Algo'][0]

assertThat(response.getHeaderFields()['Hash-Algo'][0]).isEqualTo(hashLog)

hash = response.getHeaderFields()['Hash'][0]

assertThat(response.getHeaderFields()['Hash'][0]).isEqualTo(hash)
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
