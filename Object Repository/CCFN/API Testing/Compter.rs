<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Compter</name>
   <tag></tag>
   <elementGuidId>f84092e1-8d5d-4e3f-82af-1cfe3b3395f8</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Basic bmF0YXNoYUBnbWFpbC5jb206QE9wZW5iZWU3NA==</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>${url}/ws/ccfn/inventory/count?id-ccfn=${id-ccfn}&amp;id-cont=${id-cont}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.id_ccfn</defaultValue>
      <description></description>
      <id>80b06895-53d1-4a04-b4d8-d30d6665bdac</id>
      <masked>false</masked>
      <name>id-ccfn</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.id_cont</defaultValue>
      <description></description>
      <id>64596ef5-64d6-4a25-bee8-6683d66916a7</id>
      <masked>false</masked>
      <name>id-cont</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.URL</defaultValue>
      <description></description>
      <id>108f7b22-0a56-4b6f-ac2b-18663d1c1abc</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonParser
import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

idCont = response.getHeaderFields()['Id-Cont'][0]

assertThat(response.getHeaderFields()['Id-Cont'][0]).isEqualTo(idCont)

idCcfn = response.getHeaderFields()['Id-Ccfn'][0]

assertThat(response.getHeaderFields()['Id-Ccfn'][0]).isEqualTo(idCcfn)

idUti = response.getHeaderFields()['Id-Uti'][0]

assertThat(response.getHeaderFields()['Id-Uti'][0]).isEqualTo(idUti)

date = response.getHeaderFields()['Date'][0]

assertThat(response.getHeaderFields()['Date'][0]).isEqualTo(date)

count = response.getHeaderFields()['Count'][0]

assertThat(response.getHeaderFields()['Count'][0]).isEqualTo(count)

assertThat(count).isEqualTo(response.getResponseText().valueOf(count))
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
