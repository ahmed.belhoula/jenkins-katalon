<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Deposer avec un SHA1</name>
   <tag></tag>
   <elementGuidId>05c29f4e-8d1a-48a5-8997-ec6b8b45812b</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;id-ccfn&quot;,
      &quot;value&quot;: &quot;${id-ccfn}&quot;,
      &quot;type&quot;: &quot;Text&quot;
    },
    {
      &quot;name&quot;: &quot;id-on-uti&quot;,
      &quot;value&quot;: &quot;${id-on-uti}&quot;,
      &quot;type&quot;: &quot;Text&quot;
    },
    {
      &quot;name&quot;: &quot;id-cont&quot;,
      &quot;value&quot;: &quot;${id-cont}&quot;,
      &quot;type&quot;: &quot;Text&quot;
    },
    {
      &quot;name&quot;: &quot;file&quot;,
      &quot;value&quot;: &quot;C:\\Users\\Administrateur\\Documents\\T1_000001.txt&quot;,
      &quot;type&quot;: &quot;File&quot;
    },
    {
      &quot;name&quot;: &quot;SHA&quot;,
      &quot;value&quot;: &quot;${SHA}&quot;,
      &quot;type&quot;: &quot;Text&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Basic bmF0YXNoYUBnbWFpbC5jb206QE9wZW5iZWU3NA==</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${url}/ws/ccfn/document?</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.id_ccfn</defaultValue>
      <description></description>
      <id>964af8d0-463d-43c3-93ac-70d497438a82</id>
      <masked>false</masked>
      <name>id-ccfn</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>4f76d1c0-0919-40f6-bdc4-d8230dbd6093</id>
      <masked>false</masked>
      <name>id-on-uti</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.id_cont</defaultValue>
      <description></description>
      <id>bd1a5925-646b-40a4-9754-c25d1acc6c8f</id>
      <masked>false</masked>
      <name>id-cont</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.URL</defaultValue>
      <description></description>
      <id>bbb197be-b341-466c-afd0-ef0526986fec</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.SHA1</defaultValue>
      <description></description>
      <id>c57e17c9-19e1-4f94-b8fc-9e3e8c2c0131</id>
      <masked>false</masked>
      <name>SHA</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

assertThat(response.getContentType()).isEqualTo('application/json')

idCont = response.getHeaderFields()['Id-Cont'][0]

assertThat(response.getHeaderFields()['Id-Cont'][0]).isEqualTo(idCont)

idU = response.getHeaderFields()['Idu'][0]

assertThat(response.getHeaderFields()['Idu'][0]).isEqualTo(idU)

idCcfn = response.getHeaderFields()['Id-Ccfn'][0]

assertThat(response.getHeaderFields()['Id-Ccfn'][0]).isEqualTo(idCcfn)

idOnUti = response.getHeaderFields()['Id-On-Uti'][0]

assertThat(response.getHeaderFields()['Id-On-Uti'][0]).isEqualTo(idOnUti)

idUti = response.getHeaderFields()['Id-Uti'][0]

assertThat(response.getHeaderFields()['Id-Uti'][0]).isEqualTo(idUti)

date = response.getHeaderFields()['Date'][0]

assertThat(response.getHeaderFields()['Date'][0]).isEqualTo(date)

size = response.getHeaderFields()['Size'][0]

assertThat(response.getHeaderFields()['Size'][0]).isEqualTo(size)

hashLog = response.getHeaderFields()['Hash-Algo'][0]

assertThat(response.getHeaderFields()['Hash-Algo'][0]).isEqualTo(hashLog)

hash = response.getHeaderFields()['Hash'][0]

assertThat(response.getHeaderFields()['Hash'][0]).isEqualTo(hash)
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
