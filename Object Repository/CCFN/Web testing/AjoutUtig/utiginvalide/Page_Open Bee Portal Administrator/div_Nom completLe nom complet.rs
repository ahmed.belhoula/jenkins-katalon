<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Nom completLe nom complet</name>
   <tag></tag>
   <elementGuidId>2b1a6f2e-5554-497c-8080-816467da8c6e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-5 nopadding</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	
		Nom complet:
		Le nom complet est obligatoireThe fullname is too long. 75 characters maximum
	

	
		Email:
		Email invalide
	

	
		
		
		
		Utiliser l'adresse mail comme nom d'utilisateur
	

	
	Identifiant:
		username is too long. 75 characters maximu
	

	
		
		Mot de passe
		Le mot de passe est obligatoiretaille maximale doit être inférieur à 30
	

	
		Confirmation 
		La confirmation du mot de passe est obligatoireLe mot de passe doit contenir au moins 8 caractères, dont au moins 1 lettre(s) majuscule(s) et 1 lettre(s) minuscule(s) et 1 chiffre(s) et 1 caractère(s) spécial(s)Confirmation du mot de passe est incorrecte
	

	
		Photo
		
			
			
				
				    				
				
					
                         
							
								Parcourir...															
						
						
						
			                
                                
                                
                                
                                
                            
						
					
				
            
			
		
	


	



	
	Rôle:
		   Utilisateur                      
Administrateur
Utilisateur
Externe
		user type is require
	
		
	Nbr connexions max.:
		   4                      
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
38
39
40
41
42
43
44
45
46
47
48
49
50
51
52
53
54
55
56
57
58
59
60
61
62
63
64
65
66
67
68
69
70
71
72
73
74
75
76
77
78
79
80
81
82
83
84
85
86
87
88
89
90
91
92
93
94
95
96
97
98
99
		max sessions is required
	
	
	
				Téléphone
				France+33United States+1Afghanistan (‫افغانستان‬‎)+93Albania (Shqipëri)+355Algeria (‫الجزائر‬‎)+213American Samoa+1684Andorra+376Angola+244Anguilla+1264Antigua and Barbuda+1268Argentina+54Armenia (Հայաստան)+374Aruba+297Australia+61Austria (Österreich)+43Azerbaijan (Azərbaycan)+994Bahamas+1242Bahrain (‫البحرين‬‎)+973Bangladesh (বাংলাদেশ)+880Barbados+1246Belarus (Беларусь)+375Belgium (België)+32Belize+501Benin (Bénin)+229Bermuda+1441Bhutan (འབྲུག)+975Bolivia+591Bosnia and Herzegovina (Босна и Херцеговина)+387Botswana+267Brazil (Brasil)+55British Indian Ocean Territory+246British Virgin Islands+1284Brunei+673Bulgaria (България)+359Burkina Faso+226Burundi (Uburundi)+257Cambodia (កម្ពុជា)+855Cameroon (Cameroun)+237Canada+1Cape Verde (Kabu Verdi)+238Caribbean Netherlands+599Cayman Islands+1345Central African Republic (République centrafricaine)+236Chad (Tchad)+235Chile+56China (中国)+86Christmas Island+61Cocos (Keeling) Islands+61Colombia+57Comoros (‫جزر القمر‬‎)+269Congo (DRC) (Jamhuri ya Kidemokrasia ya Kongo)+243Congo (Republic) (Congo-Brazzaville)+242Cook Islands+682Costa Rica+506Côte d’Ivoire+225Croatia (Hrvatska)+385Cuba+53Curaçao+599Cyprus (Κύπρος)+357Czech Republic (Česká republika)+420Denmark (Danmark)+45Djibouti+253Dominica+1767Dominican Republic (República Dominicana)+1Ecuador+593Egypt (‫مصر‬‎)+20El Salvador+503Equatorial Guinea (Guinea Ecuatorial)+240Eritrea+291Estonia (Eesti)+372Ethiopia+251Falkland Islands (Islas Malvinas)+500Faroe Islands (Føroyar)+298Fiji+679Finland (Suomi)+358France+33French Guiana (Guyane française)+594French Polynesia (Polynésie française)+689Gabon+241Gambia+220Georgia (საქართველო)+995Germany (Deutschland)+49Ghana (Gaana)+233Gibraltar+350Greece (Ελλάδα)+30Greenland (Kalaallit Nunaat)+299Grenada+1473Guadeloupe+590Guam+1671Guatemala+502Guernsey+44Guinea (Guinée)+224Guinea-Bissau (Guiné Bissau)+245Guyana+592Haiti+509Honduras+504Hong Kong (香港)+852Hungary (Magyarország)+36Iceland (Ísland)+354India (भारत)+91Indonesia+62Iran (‫ایران‬‎)+98Iraq (‫العراق‬‎)+964Ireland+353Isle of Man+44Israel (‫ישראל‬‎)+972Italy (Italia)+39Jamaica+1876Japan (日本)+81Jersey+44Jordan (‫الأردن‬‎)+962Kazakhstan (Казахстан)+7Kenya+254Kiribati+686Kuwait (‫الكويت‬‎)+965Kyrgyzstan (Кыргызстан)+996Laos (ລາວ)+856Latvia (Latvija)+371Lebanon (‫لبنان‬‎)+961Lesotho+266Liberia+231Libya (‫ليبيا‬‎)+218Liechtenstein+423Lithuania (Lietuva)+370Luxembourg+352Macau (澳門)+853Macedonia (FYROM) (Македонија)+389Madagascar (Madagasikara)+261Malawi+265Malaysia+60Maldives+960Mali+223Malta+356Marshall Islands+692Martinique+596Mauritania (‫موريتانيا‬‎)+222Mauritius (Moris)+230Mayotte+262Mexico (México)+52Micronesia+691Moldova (Republica Moldova)+373Monaco+377Mongolia (Монгол)+976Montenegro (Crna Gora)+382Montserrat+1664Morocco (‫المغرب‬‎)+212Mozambique (Moçambique)+258Myanmar (Burma) (မြန်မာ)+95Namibia (Namibië)+264Nauru+674Nepal (नेपाल)+977Netherlands (Nederland)+31New Caledonia (Nouvelle-Calédonie)+687New Zealand+64Nicaragua+505Niger (Nijar)+227Nigeria+234Niue+683Norfolk Island+672North Korea (조선 민주주의 인민 공화국)+850Northern Mariana Islands+1670Norway (Norge)+47Oman (‫عُمان‬‎)+968Pakistan (‫پاکستان‬‎)+92Palau+680Palestine (‫فلسطين‬‎)+970Panama (Panamá)+507Papua New Guinea+675Paraguay+595Peru (Perú)+51Philippines+63Poland (Polska)+48Portugal+351Puerto Rico+1Qatar (‫قطر‬‎)+974Réunion (La Réunion)+262Romania (România)+40Russia (Россия)+7Rwanda+250Saint Barthélemy (Saint-Barthélemy)+590Saint Helena+290Saint Kitts and Nevis+1869Saint Lucia+1758Saint Martin (Saint-Martin (partie française))+590Saint Pierre and Miquelon (Saint-Pierre-et-Miquelon)+508Saint Vincent and the Grenadines+1784Samoa+685San Marino+378São Tomé and Príncipe (São Tomé e Príncipe)+239Saudi Arabia (‫المملكة العربية السعودية‬‎)+966Senegal (Sénégal)+221Serbia (Србија)+381Seychelles+248Sierra Leone+232Singapore+65Sint Maarten+1721Slovakia (Slovensko)+421Slovenia (Slovenija)+386Solomon Islands+677Somalia (Soomaaliya)+252South Africa+27South Korea (대한민국)+82South Sudan (‫جنوب السودان‬‎)+211Spain (España)+34Sri Lanka (ශ්‍රී ලංකාව)+94Sudan (‫السودان‬‎)+249Suriname+597Svalbard and Jan Mayen+47Swaziland+268Sweden (Sverige)+46Switzerland (Schweiz)+41Syria (‫سوريا‬‎)+963Taiwan (台灣)+886Tajikistan+992Tanzania+255Thailand (ไทย)+66Timor-Leste+670Togo+228Tokelau+690Tonga+676Trinidad and Tobago+1868Tunisia (‫تونس‬‎)+216Turkey (Türkiye)+90Turkmenistan+993Turks and Caicos Islands+1649Tuvalu+688U.S. Virgin Islands+1340Uganda+256Ukraine (Україна)+380United Arab Emirates (‫الإمارات العربية المتحدة‬‎)+971United Kingdom+44United States+1Uruguay+598Uzbekistan (Oʻzbekiston)+998Vanuatu+678Vatican City (Città del Vaticano)+39Venezuela+58Vietnam (Việt Nam)+84Wallis and Futuna+681Western Sahara (‫الصحراء الغربية‬‎)+212Yemen (‫اليمن‬‎)+967Zambia+260Zimbabwe+263Åland Islands+358Le numéro de votre Téléphone portable est non valide
			
			
		
	
		Double facteur d'authentification:
		   Désactivé                      
Désactivé
Par email
		
	

	
		
			
				Notifications par email
				
			
		
		
			
				
					Fréquence de notification des workflows
					
Jamais
Immédiatement 
Une fois par jour
Une fois par semaine
Une fois par mois

				
				
					Fréquence de notification des tâches
					
Jamais
Immédiatement 
Une fois par jour
Une fois par semaine
Une fois par mois

				
				
					Fréquence de notification des tâches en retard
					
Jamais
Une fois par jour
Une fois par semaine
Une fois par mois

				
				
					Fréquence de notification des rappels 
					
Jamais
Immédiatement 
Une fois par jour
Une fois par semaine
Une fois par mois

				
				
					Fréquence de notification des souscriptions
					
Jamais
Immédiatement 
Une fois par jour
Une fois par semaine
Une fois par mois

				
				
					Fréquence de notification des commentaires
					
Jamais
Immédiatement 
Une fois par jour
Une fois par semaine
Une fois par mois

				
				
					Fréquence de notification des documents à classer
					
Jamais
Immédiatement 
Une fois par jour
Une fois par semaine
Une fois par mois

				
			
		
	


	
		
	

		
		 
	
	
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;addUserForm&quot;)/div[@class=&quot;col-md-5 nopadding&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//form[@id='addUserForm']/div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ajouter un utilisateur'])[1]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Horaires de travail'])[1]/following::div[9]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//form/div[3]</value>
   </webElementXpaths>
</WebElementEntity>
