import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def data1 = findTestData('Data Files/Data1')

def data2 = findTestData('Data Files/Data2')

WebUI.navigateToUrl('http://10.12.1.20/')

//for (i = 1; i < data1.getRowNumbers() ; i++) {

WebUI.setText(findTestObject('Object Repository/AffectUTIF/Page_Open Bee Portal/input_concat(Nom d  utilisateu'), 'Utig')

WebUI.setEncryptedText(findTestObject('Object Repository/AffectUTIF/Page_Open Bee Portal/input_Mot de passe_signinpassw'), 
    '/sQ3/mmFm9W9ZAXSh+fndA==')

WebUI.click(findTestObject('Object Repository/AffectUTIF/Page_Open Bee Portal/button_Connexion'))

WebUI.setText(findTestObject('Object Repository/AffectUTIF/Page_Open Bee Portal/input_Code_code'), '95798')

WebUI.click(findTestObject('Object Repository/AffectUTIF/Page_Open Bee Portal/input_concat(Dsactiver le seco'))

WebUI.click(findTestObject('Object Repository/AffectUTIF/Page_Open Bee Portal Utig/i__ob ob-s ob-menu-burger'))

WebUI.click(findTestObject('Object Repository/AffectUTIF/Page_Open Bee Portal Utig/span_ESC'))

WebUI.click(findTestObject('Object Repository/AffectUTIF/Page_Open Bee Portal Utig/span_Nombre de conteneurs_care'))

WebUI.click(findTestObject('Object Repository/AffectUTIF/Page_Open Bee Portal Utig/a_Grer les administrateurs fon'))

WebUI.click(findTestObject('Object Repository/AffectUTIF/Page_Open Bee Portal Utig/div_Administrateurs fonctionne'))

WebUI.click(findTestObject('Object Repository/AffectUTIF/Page_Open Bee Portal Utig/div_Marie'))

WebUI.click(findTestObject('Object Repository/AffectUTIF/Page_Open Bee Portal Utig/input_Marie_save'))

