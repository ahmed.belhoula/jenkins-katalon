import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver

WebUI.navigateToUrl('http://10.12.1.20/')

def data2 = findTestData('Data Files/Data2')

WebDriver driver = DriverFactory.getWebDriver()

WebUI.setText(findTestObject('CCFN/DesactiverDoubleAuth/Page_Open Bee Portal/input_concat(Nom d  utilisateu'), 'admin')

WebUI.setEncryptedText(findTestObject('CCFN/DesactiverDoubleAuth/Page_Open Bee Portal/input_Mot de passe_signinpassw'), 
    'RAIVpflpDOg=')

WebUI.click(findTestObject('CCFN/DesactiverDoubleAuth/Page_Open Bee Portal/button_Connexion'))

WebUI.click(findTestObject('CCFN/DesactiverDoubleAuth/Page_Open Bee Portal Administrator/i_Dconnexion_administrationTou'))

WebUI.click(findTestObject('CCFN/DesactiverDoubleAuth/Page_Open Bee Portal Administrator/a_Utilisateurs'))

WebElement Table = driver.findElement(By.xpath('//*[@id="form_users"]/table'))

List<WebElement> rows_table = Table.findElements(By.tagName('tr'))

int rows_count = rows_table.size()

int row = 2

for (i = 1; i < (data2.getRowNumbers() + 1); i++) {
    while (row < rows_count) {
        if (rows_table.get(row).findElements(By.tagName('td')).get(3).getText() == 'Utig') {
            String varIdDropdown = rows_table.get(row).findElement(By.tagName('button')).getAttribute('id')

            Nom = WebUI.modifyObjectProperty(findTestObject('CCFN/DesactiverDoubleAuth/Page_Open Bee Portal Administrator/button_Actif_dropdownActionLis'), 
                'id', 'equals', varIdDropdown, true)

            WebUI.click(Nom)

            String ch = 'dropdownActionList'

            String varNumEdit = Nom.findPropertyValue('id').substring(ch.length(), Nom.findPropertyValue('id').length())

            String varIdEdit = 'editUserAction' + varNumEdit

            Edit = WebUI.modifyObjectProperty(findTestObject('CCFN/DesactiverDoubleAuth/Page_Open Bee Portal Administrator/a_Editer'), 
                'id', 'equals', varIdEdit, true)

            WebUI.click(Edit)

            WebUI.click(findTestObject('CCFN/DesactiverDoubleAuth/Page_Open Bee Portal Administrator/div_Par email'))

            WebUI.selectOptionByLabel(findTestObject('CCFN/DesactiverDoubleAuth/Page_Open Bee Portal Administrator/div_Dsactiv'), 
                'Désactivé', false)

            WebUI.click(findTestObject('CCFN/DesactiverDoubleAuth/Page_Open Bee Portal Administrator/input_concat(Affecter l  utili'))

            WebUI.rightClick(findTestObject('CCFN/DesactiverDoubleAuth/Page_Open Bee Portal Administrator/div_Lopration a t effectue ave'))

            WebUI.click(findTestObject('CCFN/DesactiverDoubleAuth/Page_Open Bee Portal Administrator/img_Administrator_round img-re'))

            WebUI.delay(3)

            row = rows_count
        }
        
        row++
    }
}