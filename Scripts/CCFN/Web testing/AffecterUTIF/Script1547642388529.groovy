import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import javax.xml.xpath.XPath as XPath
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By
import org.openqa.selenium.firefox.FirefoxDriver as FirefoxDriver
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://10.12.1.20/')

def data2 = findTestData('utif')

String ch

WebUI.setText(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/Login'), 'Utig')

WebUI.setEncryptedText(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/Password'), '/sQ3/mmFm9W9ZAXSh+fndA==')

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/button_Connexion'))

String ch1 = WebUI.getText(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal Administrator/StronAuth')).substring(
    49)

System.setProperty('webdriver.gecko.driver', 'C:\\Katalon_Studio\\configuration\\resources\\drivers\\firefox_win64\\geckodriver.exe')
WebDriver driver = new FirefoxDriver()

DriverFactory.changeWebDriver(driver)

WebUI.navigateToUrl('http://10.12.1.20/')

WebUI.setText(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/Login'), 'admin')

WebUI.setEncryptedText(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/Password'), 'RAIVpflpDOg=')

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/button_Connexion'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal Utig/AdministrationTour'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal Utig/a_Emails'))

WebElement Table = driver.findElement(By.xpath('//*[@id="form_emails"]/table'))

List<WebElement> rows_table = Table.findElements(By.tagName('tr'))

int rows_count = rows_table.size()

int row = 2

while (row < rows_count) {
    String var = rows_table.get(row).findElements(By.tagName('td')).get(4).getText()

    if (var.substring(2) == ch1) {
        ch = rows_table.get(row).findElements(By.tagName('td')).get(7).getText().substring(38)

        row = rows_count
    }
    
    row++
}

WebUI.closeBrowser()

WebUI.setText(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/InputCode'), ch)

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/ConnexionDoubAuth'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal Utig/MenuBurger'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal Utig/SpanESC'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal Utig/DropDownUtif'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal Utig/GererUtif'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal Utig/ChooseUtif'))

WebDriver driver1 = DriverFactory.getWebDriver()

for (i = 1; i < data2.getRowNumbers(); i++) {
    WebElement utif = driver1.findElement(By.xpath('//*[@id="functionalAdmins"]/div[3]'))

    List<WebElement> rows = utif.findElements(By.tagName('div'))

    int rows_countt = rows.size()

    int row1 = 2

    while (row1 < rows_countt) {
        String var = rows.get(row1).getText()

        if (var == data2.getValue(3, i)) {
            println(var)

            rows.get(row1).click()

            row1 = rows_countt

            WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal Utig/ChooseUtif'))
        }
        
        row1++
    }
}

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal Utig/SaveUtif'))

WebUI.waitForElementVisible(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal utif1/notificationSuccess'), 
    2)

 driver1.close()



