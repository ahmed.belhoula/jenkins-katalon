import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def data = findTestData('ESC')

def data1 = findTestData('utig')

WebUI.setText(findTestObject('CCFN/Web testing/AjoutESC/Page_Open Bee Portal/Login'), 'admin')

WebUI.setEncryptedText(findTestObject('CCFN/Web testing/AjoutESC/Page_Open Bee Portal/Password'), 'RAIVpflpDOg=')

WebUI.click(findTestObject('CCFN/Web testing/AjoutESC/Page_Open Bee Portal/ButtonConnexion'))

WebUI.click(findTestObject('CCFN/Web testing/AjoutESC/Page_Open Bee Portal Administrator/Administration'))

WebUI.click(findTestObject('CCFN/Web testing/AjoutESC/Page_Open Bee Portal Administrator/EspaceScurisedeConservation'))

for (i = 1; i < data.getRowNumbers(); i++) {
    WebUI.click(findTestObject('CCFN/Web testing/AjoutESC/Page_Open Bee Portal Administrator/AjouterESC'))

    WebUI.click(findTestObject('CCFN/Web testing/AjoutESC/Page_Open Bee Portal Administrator/ButtonBrowse'))

    WebUI.click(findTestObject('CCFN/Web testing/AjoutESC/Page_Open Bee Portal Administrator/SpanAddFolder'))

    WebUI.delay(2)

    WebUI.setText(findTestObject('CCFN/Web testing/AjoutESC/Page_Open Bee Portal Administrator/InputFolderName'), data.getValue(
            1, i))

    WebUI.click(findTestObject('CCFN/Web testing/AjoutESC/Page_Open Bee Portal Administrator/Form'))

    spn = WebUI.modifyObjectProperty(findTestObject('CCFN/Web testing/AjoutESC/Page_Open Bee Portal Administrator/Span'), 
        'text', 'equals', data.getValue(1, i), true)

    WebUI.click(spn)

    WebUI.click(findTestObject('CCFN/Web testing/AjoutESC/Page_Open Bee Portal Administrator/ButtonSave'))

    WebUI.delay(1)

    WebUI.click(findTestObject('CCFN/Web testing/AjoutESC/Page_Open Bee Portal Administrator/ChooseUtig'))

    Utig = WebUI.modifyObjectProperty(findTestObject('CCFN/Web testing/AjoutESC/Page_Open Bee Portal Administrator/Utig'), 
        'text', 'equals', data1.getValue(1, i), true)

    WebUI.click(Utig)

    WebUI.waitForElementPresent(findTestObject('CCFN/Web testing/AjoutESC/Page_Open Bee Portal Administrator/SaveESC'), 
        3)

    WebUI.click(findTestObject('CCFN/Web testing/AjoutESC/Page_Open Bee Portal Administrator/SaveESC'))

    WebUI.waitForElementPresent(findTestObject('CCFN/Web testing/AjoutESC/Page_Open Bee Portal Administrator/ALERT'), 5)

    WebUI.delay(10)
}

