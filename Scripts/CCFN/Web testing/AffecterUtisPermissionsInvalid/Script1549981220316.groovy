import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.firefox.FirefoxDriver as FirefoxDriver

String ch

def data2 = findTestData('containers')

WebUI.setText(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal/input_concat(Nom d  utilisateu'), 'Utif1')

WebUI.setEncryptedText(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal/input_Mot de passe_signinpassw'), 
    '/sQ3/mmFm9W9ZAXSh+fndA==')

WebUI.click(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal/button_Connexion'))

String ch1 = WebUI.getText(findTestObject('Object Repository/CCFN/Web testing/AffecterUtis/Page_Open Bee Portal/StronAuth')).substring(
    49)

System.setProperty('webdriver.gecko.driver', 'C:\\Katalon_Studio\\configuration\\resources\\drivers\\firefox_win64\\geckodriver.exe')
WebDriver driver = new FirefoxDriver()

DriverFactory.changeWebDriver(driver)

WebUI.navigateToUrl('http://10.12.1.20/')

WebUI.setText(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/Login'), 'admin')

WebUI.setEncryptedText(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/Password'), 'RAIVpflpDOg=')

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/button_Connexion'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal Utig/AdministrationTour'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal Utig/a_Emails'))

WebElement Table = driver.findElement(By.xpath('//*[@id="form_emails"]/table'))

List<WebElement> rows_table = Table.findElements(By.tagName('tr'))

int rows_count = rows_table.size()

int row = 2

while (row < rows_count) {
    String var = rows_table.get(row).findElements(By.tagName('td')).get(4).getText()

    if (var.substring(2) == ch1) {
        ch = rows_table.get(row).findElements(By.tagName('td')).get(7).getText().substring(38)

        row = rows_count
    }
    
    row++
}

WebUI.closeBrowser()



WebUI.verifyElementPresent(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal/input_Code_code'), 3)

WebUI.setText(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal/input_Code_code'), ch)

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/ConnexionDoubAuth'))

WebUI.click(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal utif1/i__ob ob-s ob-menu-burger'))

WebUI.click(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal utif1/span_ESC'))

WebUI.click(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal utif1/button_Nombre de conteneurs_dr'))

WebUI.click(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal utif1/a_Ajouter un conteneur'))

WebUI.setText(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal utif1/input_Nom_name'), data2.getValue(
        1, 3))

WebUI.click(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal utif1/input_Nom_save'))

WebUI.delay(1)

WebUI.click(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal utif1/a_TEST'))

WebUI.click(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal utif1/span_Chemin_caret'))

WebUI.click(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal utif1/a_Grer les permissions'))

WebUI.click(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal utif1/addNewRow'))

WebUI.click(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal utif1/chooseExternalUser'))

WebUI.click(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal utif1/externalUser'))

WebUI.delay(1)

WebUI.click(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal utif1/input_Contrler_save'))

WebUI.verifyElementPresent(findTestObject('CCFN/Web testing/AffecterUtis/Page_Open Bee Portal utif1/notificationDanger'), 
    1)

WebDriver driver1 = DriverFactory.getWebDriver()

driver1.close()

