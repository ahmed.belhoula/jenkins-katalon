import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Date date1 = new Date()
IdOnUti1 = "Document".concat(date1.format("yyyy-MM-dd HH-mm-ss.SSS"))
println IdOnUti1

response = WS.sendRequestAndVerify(findTestObject('Object Repository/CCFN/API Testing/Deposer avec un SHA1', [('id-ccfn') : GlobalVariable.id_ccfn
            , ('id-on-uti') : IdOnUti1, ('id-cont') : GlobalVariable.id_cont, ('url') : GlobalVariable.URL, ('SHA') : GlobalVariable.SHA1]))

WS.verifyResponseStatusCode(response, 201)

Date date2 = new Date()
IdOnUti2 = "Document".concat(date2.format("yyyy-MM-dd HH-mm-ss.SSS"))
println IdOnUti2

response = WS.sendRequestAndVerify(findTestObject('Object Repository/CCFN/API Testing/Deposer avec un SHA256', [('id-ccfn') : GlobalVariable.id_ccfn
            , ('id-on-uti') : IdOnUti2, ('id-cont') : GlobalVariable.id_cont, ('url') : GlobalVariable.URL, ('SHA') : GlobalVariable.SHA256]))

WS.verifyResponseStatusCode(response, 201)

response = WS.sendRequestAndVerify(findTestObject('Object Repository/CCFN/API Testing/Deposer avec un SHA erronee', [('id-ccfn') : GlobalVariable.id_ccfn
            , ('id-on-uti') : findTestData('Document names').getValue(1, 1), ('id-cont') : GlobalVariable.id_cont, ('url') : GlobalVariable.URL, ('SHA') : GlobalVariable.SHA_erronee]))

WS.verifyResponseStatusCode(response, 400)

response = WS.sendRequestAndVerify(findTestObject('Object Repository/CCFN/API Testing/Deposer avec id-cont incorrect', [('id-ccfn') : GlobalVariable.id_ccfn
            , ('id-on-uti') : findTestData('Document names').getValue(1, 1), ('id-cont') : GlobalVariable.id_cont_invalide
            , ('url') : GlobalVariable.URL]))

WS.verifyResponseStatusCode(response, 403)

response = WS.sendRequestAndVerify(findTestObject('Object Repository/CCFN/API Testing/Deposer sans id-ccfn', [('url') : GlobalVariable.URL
            , ('id-cont') : GlobalVariable.id_cont, ('id-on-uti') : findTestData('Document names').getValue(1, 1)]))

WS.verifyResponseStatusCode(response, 400)

response = WS.sendRequestAndVerify(findTestObject('Object Repository/CCFN/API Testing/Lire journal', [('id-ccfn') : GlobalVariable.id_ccfn
            , ('id-cont') : GlobalVariable.id_cont, ('url') : GlobalVariable.URL]))

WS.verifyResponseStatusCode(response, 200)

