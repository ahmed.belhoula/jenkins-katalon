import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Date date1 = new Date()
IdOnUti1 = "Document".concat(date1.format("yyyy-MM-dd HH-mm-ss.SSS"))
println IdOnUti1

response = WS.sendRequestAndVerify(findTestObject('Object Repository/CCFN/API Testing/Deposer avec succes', [('id-ccfn') : GlobalVariable.id_ccfn
            , ('id-on-uti') : IdOnUti1, ('id-cont') : GlobalVariable.id_cont, ('url') : GlobalVariable.URL]))

WS.verifyResponseStatusCode(response, 201)

Idu = ((response.getHeaderFields()['Idu'])[0])

response = WS.sendRequestAndVerify(findTestObject('Object Repository/CCFN/API Testing/Check document', [('url') : GlobalVariable.URL, ('id-uti') : GlobalVariable.id_uti
            , ('id-ccfn') : GlobalVariable.id_ccfn, ('idu') : Idu]))

WS.verifyResponseStatusCode(response, 200)

response = WS.sendRequestAndVerify(findTestObject('Object Repository/CCFN/API Testing/Check document sans permissions', [('url') : GlobalVariable.URL
            , ('id-uti') : GlobalVariable.id_uti, ('id-ccfn') : GlobalVariable.id_ccfn, ('idu') : Idu]))

WS.verifyResponseStatusCode(response, 403)

response = WS.sendRequest(findTestObject('Object Repository/CCFN/API Testing/Check document', [('url') : GlobalVariable.URL, ('id-uti') : GlobalVariable.id_uti
            , ('id-ccfn') : GlobalVariable.id_ccfn, ('idu') : 0]))

WS.verifyResponseStatusCode(response, 400)

