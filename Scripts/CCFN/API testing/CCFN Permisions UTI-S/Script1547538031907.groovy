import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import javax.xml.xpath.XPath as XPath
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By
import org.openqa.selenium.firefox.FirefoxDriver as FirefoxDriver

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

WebUI.navigateToUrl('http://10.12.1.20/')

String ch

WebUI.setText(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/Login'), 'marie')

WebUI.setEncryptedText(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/Password'), '/sQ3/mmFm9XTCy0oyB1ULA==')

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/button_Connexion'))

WebUI.waitForElementVisible(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal Administrator/StronAuth'), 
    2)

String ch1 = WebUI.getText(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal Administrator/StronAuth')).substring(
    49)

System.setProperty('webdriver.gecko.driver', 'C:\\Katalon_Studio\\configuration\\resources\\drivers\\firefox_win64\\geckodriver.exe')

WebDriver driver = new FirefoxDriver()

DriverFactory.changeWebDriver(driver)

WebUI.navigateToUrl('http://10.12.1.20/')

WebUI.setText(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/Login'), 'admin')

WebUI.setEncryptedText(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/Password'), 'RAIVpflpDOg=')

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/button_Connexion'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal Utig/AdministrationTour'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal Utig/a_Emails'))

WebElement Table = driver.findElement(By.xpath('//*[@id="form_emails"]/table'))

List<WebElement> rows_table = Table.findElements(By.tagName('tr'))

int rows_count = rows_table.size()

int row = 2

while (row < rows_count) {
    String var = rows_table.get(row).findElements(By.tagName('td')).get(4).getText()

    if (var.substring(2) == ch1) {
        ch = rows_table.get(row).findElements(By.tagName('td')).get(7).getText().substring(38)

        row = rows_count
    }
    
    row++
}


WebUI.closeBrowser()


WebUI.waitForElementVisible(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/InputCode'), 5)

WebUI.setText(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/InputCode'), ch)

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/AffectUTIF/Page_Open Bee Portal/ConnexionDoubAuth'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/menu-burger'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/span_ESC'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/dropdownUtig'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/a_Grer les conteneurs'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/dropdownContainers'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/a_Grer les permissions'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/input_permissionsIds'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/dropdownPermissions'))

WebUI.waitForElementVisible(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/input_Deposer_permissions'), 
    3)

WebUI.check(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/input_Deposer_permissions'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/save_permissions'))

Date date1 = new Date()
IdOnUti1 = "Document".concat(date1.format("yyyy-MM-dd HH-mm-ss.SSS"))
println IdOnUti1

response = WS.sendRequestAndVerify(findTestObject('Object Repository/CCFN/API Testing/Deposer avec succes', [('id-ccfn') : GlobalVariable.id_ccfn
            , ('id-on-uti') : IdOnUti1, ('id-cont') : GlobalVariable.id_cont, ('url') : GlobalVariable.URL]))

WS.verifyResponseStatusCode(response, 201)

Idu = ((response.getHeaderFields()['Idu'])[0])

response = WS.sendRequest(findTestObject('Object Repository/CCFN/API Testing/Lire journal', [('id-ccfn') : GlobalVariable.id_ccfn
            , ('id-cont') : GlobalVariable.id_cont, ('url') : GlobalVariable.URL]))

WS.verifyResponseStatusCode(response, 403)

response = WS.sendRequest(findTestObject('Object Repository/CCFN/API Testing/Detruire', [('url') : GlobalVariable.URL, ('id-ccfn') : GlobalVariable.id_ccfn
            , ('id-cont') : GlobalVariable.id_cont, ('idu') : Idu]))

WS.verifyResponseStatusCode(response, 403)

response = WS.sendRequest(findTestObject('Object Repository/CCFN/API Testing/Lister', [('url') : GlobalVariable.URL, ('id-cont') : GlobalVariable.id_cont
            , ('id-ccfn') : GlobalVariable.id_ccfn]))

WS.verifyResponseStatusCode(response, 403)

response = WS.sendRequest(findTestObject('Object Repository/CCFN/API Testing/Compter', [('id-ccfn') : GlobalVariable.id_ccfn
            , ('id-cont') : GlobalVariable.id_cont, ('url') : GlobalVariable.URL]))

WS.verifyResponseStatusCode(response, 403)

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/dropdownContainers'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/a_Grer les permissions'))

WebUI.waitForElementVisible(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/input_permissionsIds'),
	3)

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/input_permissionsIds'))

WebUI.waitForElementVisible(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/dropdownPermissions'),
	3)

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/dropdownPermissions'))

WebUI.waitForElementVisible(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/input_Deposer_permissions'), 
    3)

WebUI.uncheck(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/input_Deposer_permissions'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/save_permissions'))

Date date2 = new Date()
IdOnUti2 = "Document".concat(date2.format("yyyy-MM-dd HH-mm-ss.SSS"))
println IdOnUti2

response = WS.sendRequest(findTestObject('Object Repository/CCFN/API Testing/Deposer avec succes', [('id-ccfn') : GlobalVariable.id_ccfn
            , ('id-on-uti') : IdOnUti2, ('id-cont') : GlobalVariable.id_cont, ('url') : GlobalVariable.URL]))

WS.verifyResponseStatusCode(response, 403)

response = WS.sendRequest(findTestObject('Object Repository/CCFN/API Testing/Lister', [('url') : GlobalVariable.URL, ('id-cont') : GlobalVariable.id_cont
            , ('id-ccfn') : GlobalVariable.id_ccfn]))

WS.verifyResponseStatusCode(response, 200)

response = WS.sendRequest(findTestObject('Object Repository/CCFN/API Testing/Compter', [('id-ccfn') : GlobalVariable.id_ccfn
            , ('id-cont') : GlobalVariable.id_cont, ('url') : GlobalVariable.URL]))

WS.verifyResponseStatusCode(response, 200)

response = WS.sendRequest(findTestObject('Object Repository/CCFN/API Testing/Lire journal', [('id-ccfn') : GlobalVariable.id_ccfn
            , ('id-cont') : GlobalVariable.id_cont, ('url') : GlobalVariable.URL]))

WS.verifyResponseStatusCode(response, 200)

response = WS.sendRequest(findTestObject('Object Repository/CCFN/API Testing/Detruire', [('url') : GlobalVariable.URL, ('id-ccfn') : GlobalVariable.id_ccfn
            , ('id-cont') : GlobalVariable.id_cont, ('idu') : Idu]))

WS.verifyResponseStatusCode(response, 200)

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/dropdownContainers'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/a_Grer les permissions'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/input_permissionsIds'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/dropdownPermissions'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/save_permissions'))

WebUI.delay(2)

WebUI.verifyElementClickable(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/preferences_DropdownList'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/preferences_DropdownList'))

WebUI.click(findTestObject('Object Repository/CCFN/Web testing/Permissions UTI-S/a_deconnexion'))

WebUI.closeBrowser()

