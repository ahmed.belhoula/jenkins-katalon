import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.navigateToUrl('http://10.12.1.20/')

WebUI.setText(findTestObject('CCFN/AjoutUtig/utigvalide/Page_Open Bee Portal/input_concat(Nom d  utilisateu'), 'admin')

WebUI.setEncryptedText(findTestObject('CCFN/AjoutUtig/utigvalide/Page_Open Bee Portal/input_Mot de passe_signinpassw'), 
    'RAIVpflpDOg=')

WebUI.click(findTestObject('CCFN/AjoutUtig/utigvalide/Page_Open Bee Portal/button_Connexion'))

WebUI.click(findTestObject('CCFN/AjoutUtig/utigvalide/Page_Open Bee Portal Administrator/i_Dconnexion_administrationTou'))

WebUI.click(findTestObject('CCFN/AjoutUtig/utigvalide/Page_Open Bee Portal Administrator/a_Utilisateurs'))

def data1 = findTestData('Data Files/Data1')

for (i = 1; i < data1.getRowNumbers(); i++) {
    WebUI.click(findTestObject('CCFN/AjoutUtig/utigvalide/Page_Open Bee Portal Administrator/button_Ajouter un utilisateur'))

    WebUI.setText(findTestObject('CCFN/AjoutUtig/utigvalide/Page_Open Bee Portal Administrator/input_Nom complet_obpUserfulln'), 
        data1.getValue(1, i))

    WebUI.setText(findTestObject('CCFN/AjoutUtig/utigvalide/Page_Open Bee Portal Administrator/input_Email_obpUseremail'), 
        data1.getValue(3, i))

    WebUI.click(findTestObject('CCFN/AjoutUtig/utigvalide/Page_Open Bee Portal Administrator/input_Email invalide_useMail'))

    WebUI.setText(findTestObject('CCFN/AjoutUtig/utigvalide/Page_Open Bee Portal Administrator/input_Identifiant_obpUserusern'), 
        data1.getValue(2, i))

    WebUI.setEncryptedText(findTestObject('CCFN/AjoutUtig/utigvalide/Page_Open Bee Portal Administrator/input_Mot de passe_obpUserpass'), 
        '/sQ3/mmFm9W9ZAXSh+fndA==')

    WebUI.setEncryptedText(findTestObject('CCFN/AjoutUtig/utigvalide/Page_Open Bee Portal Administrator/input_Confirmation_obpUserrepa'), 
        '/sQ3/mmFm9W9ZAXSh+fndA==')

    WebUI.click(findTestObject('CCFN/AjoutUtig/utigvalide/Page_Open Bee Portal Administrator/input_Frquence de notification'))

    not_run: WebUI.click(findTestObject('CCFN/AjoutUtig/utigvalide/Page_Open Bee Portal Administrator/div_Utilisateur ajout avec suc'))

    WebUI.waitForElementPresent(findTestObject('CCFN/AjoutESC/Page_Open Bee Portal Administrator/ALERT'), 5)
}

